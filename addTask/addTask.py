# По всем вопросам обращаться:
# telegram: @vvozian
# vk.com/vozianv

# Демо версия. Код не претендует на production

import socket
import threading
import json
import time

tasks = {}

def getLocalIp():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 80))
    ip = s.getsockname()[0]
    s.close()
    del s
    return ip

def taskResender():
    while 1:
        currentTime = time.time()
        for tempTask in list(tasks):
            if currentTime - int(tasks[tempTask]["timestamp"]) > 10:
                ret = tasks[tempTask]
                ret["taskId"] = tempTask
                ret["datatype"] = "newTask"
                # print(f"Task {tempTask} was resent to network because of passed time.")
                sor.sendto((json.dumps(ret)).encode('utf-8'), ("<broadcast>", 37020))
        time.sleep(10)

def receive():
    while 1:
        data, addr = sor.recvfrom(1024)
        data = data.decode("utf-8")
        if addr[0] != localIp:
            try:
                data = json.loads(data)
                if "datatype" in data:
                    if data["datatype"] == "pickTask":
                        if "taskId" in data and "timestamp" in data:
                            rTaskId = data["taskId"]
                            rTimestamp = data["timestamp"]
                            if rTaskId in tasks:
                                if str(rTimestamp) == str(tasks[rTaskId]["timestamp"]):
                                    ret = {}
                                    ret["datatype"] = "approvedTask"
                                    ret["task"] = tasks[rTaskId]
                                    ret["taskId"] = rTaskId
                                    sor.sendto((json.dumps(ret)).encode('utf-8'), (addr[0], 37020))
                                    # print(f"Task (Task id: {rTaskId}) confirmed for drone {addr[0]}")
                                    del tasks[rTaskId]
                                else:
                                    ret = {}
                                    ret["datatype"] = "error"
                                    ret["data"] = 1  # timestamp or task error
                                    sor.sendto((json.dumps(ret)).encode('utf-8'), (addr[0], 37020))
                            else:
                                ret = {}
                                ret["datatype"] = "error"
                                ret["data"] = 1  # timestamp or task error
                                sor.sendto((json.dumps(ret)).encode('utf-8'), (addr[0], 37020))
                        else:
                            ret = {}
                            ret["datatype"] = "error"
                            ret["data"] = 1  # timestamp or task error
                            sor.sendto((json.dumps(ret)).encode('utf-8'), (addr[0], 37020))
                    
                else:
                    ret = {}
                    ret["datatype"] = "error"
                    ret["data"] = 1  # timestamp or task error
                    sor.sendto((json.dumps(ret)).encode('utf-8'), (addr[0], 37020))
            except:
                print("error")
                ret = {}
                ret["datatype"] = "error"
                ret["data"] = 1 #timestamp or task error
                sor.sendto((json.dumps(ret)).encode('utf-8'), (addr[0], 37020))




localIp = getLocalIp()
sor = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
sor.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sor.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
sor.bind(('', 37020)) # Задаем сокет как клиент
sor.sendto(('Connect to server').encode('utf-8'), ("<broadcast>", 37020))# Уведомляем сервер о подключении
potok = threading.Thread(target=receive)
potok.start()
tasksResending = threading.Thread(target=taskResender)
tasksResending.start()
print("\n"*2)
print("To add task type: addtask %taskId% %departure% %destination%\nFor example:\n"+"-"*6+"\n"+"2a23fac d b\n"+"-"*6)
print("To see not taken tasks, type: tasks")
print("\n"*2)
while 1 :
    t = input("->: ")
    try:
        t = t.split(" ")
        if t[0] == "addtask":
            taskId = t[1]
            taskPath = {"from": t[2], "to": t[3]}
            if taskId not in list(tasks) and taskId != "" and t[2] != "" and t[3] != "":
                ret = {}
                ret["datatype"] = "newTask"
                ret["from"] = taskPath["from"]
                ret["to"] = taskPath["to"]
                ret["taskId"] = str(taskId)
                ret["timestamp"] = int(time.time())
                sor.sendto((json.dumps(ret)).encode('utf-8'), ("<broadcast>", 37020))
                tasks[taskId] = {"timestamp": ret["timestamp"], "from": taskPath["from"], "to": taskPath["to"]}
        elif t[0] == "tasks":
            for item in list(tasks):
                print(f"  - id: {item},  from: {tasks[item]['from']},  to:{tasks[item]['to']}")
    except:
        pass