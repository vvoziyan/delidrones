# По всем вопросам обращаться:
# telegram: @vvozian
# vk.com/vozianv

# Демо версия. Код не претендует на production

import socket
import threading
import json
import time
import random

def getLocalIp():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 80))
    ip = s.getsockname()[0]
    s.close()
    del s
    return ip


localIp = getLocalIp()
drone_Speed = 1.39
task = None
path = None
dists = None

currentAnchorVertice = random.choices(["a", "b", "c", "d", "e"])[0]

sor = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sor.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sor.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
sor.bind(("", 37020))


print(f"Generated new drone: (IP: {localIp}) (Start position: {currentAnchorVertice})")

while 1:
    # Wait for task
    while task == None:
        data, addr = sor.recvfrom(4096)
        data = data.decode("utf-8")
        try:
            data = json.loads(data)
            if "datatype" in data: 
                if data["datatype"] == "newTask" and "taskId" in data and "from" in data and "to" in data and "timestamp" in data:
                    if currentAnchorVertice == data["from"]:
                        sor.sendto((json.dumps({"datatype": "pickTask", "timestamp": data["timestamp"], "taskId": data["taskId"]})).encode('utf-8'), (addr[0], 37020))
                        data, addr = sor.recvfrom(4096)
                        data = data.decode("utf-8")
                        try:
                            data = json.loads(data)
                            if "datatype" in data:
                                if data["datatype"] == "approvedTask" and "task" in data:
                                    task = data["task"]
                                    task["taskId"] = data["taskId"]
                                    print(f"Took task: From ({task['from']}) to ({task['to']}) with id ({task['taskId']})")

                        except:
                            pass
        except:
            pass

    if task != None:
        sor.sendto((json.dumps({"datatype": "getPath", "from": task["from"], "to": task["to"]})).encode('utf-8'), ("<broadcast>", 37020))
        isNoReturn = True
        while isNoReturn:
            data, addr = sor.recvfrom(4096)
            data = data.decode("utf-8")
            if addr[0] != localIp:
                isNoReturn = False
        try:
            data = json.loads(data)
            if "datatype" in data:
                if data["datatype"] == "pathReturn" and "path" in data and "dists" in data:
                    print("Got path: ", data["path"])
                    path = data["path"]
                    dists = data["dists"]
        except:
            pass

    if task != None and path != None and dists != None:
        for i in range(len(path)-1):
            onTrip = {"from": path[i], "to": path[i+1]}
            tempDist = dists[i]
            while tempDist>0:
                tempDist -= drone_Speed
                time.sleep(1)
                if tempDist >=-0:
                    print(f"Go from: {onTrip['from']} to: {onTrip['to']}, remaining way: {tempDist}")
                else:
                    print(f"Go from: {onTrip['from']} to: {onTrip['to']}, remaining way: 0")
            currentAnchorVertice = path[i+1]
        print(f"Task ended. Current position: {currentAnchorVertice}")
        task = None
        path = None
        dists = None


            
    
