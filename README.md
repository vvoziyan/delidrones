# Работа со средой для дронов-курьеров
Рекомендуемые версии программ и ОС:  
* linux (Ubuntu 18.04)  
* docker (v19.03.6)  
## Сборка Docker контейнеров
Находясь в корне клонированного репозитория: [https://vvoziyan@bitbucket.org/vvoziyan/delidrones.git](https://vvoziyan@bitbucket.org/vvoziyan/delidrones.git)  
(Репозиторий дублируется в виде zip архива, в приложениях к работе)
```console
docker build -t addtask ./addTask/
docker build -t pathdelive ./pathDelive/
docker build -t carrydrone ./deliCopter/
```
## Запуск контейнеров
Запуск контейнеров производится строго в следующем порядке:

Запуск контейнера(ов) `симуляции дрона`.
```console
docker run -t carrydrone
```
Запуск контейнера для `создания задач` на доставку
```console
docker run -it addtask
```
Запуск контейнера `отдачи маршрутов`
```console
docker run -t pathdelive
```
Так как контейнеры во время работы выводят значения, помогающие определить состояние системы, то при запуске каждого контейнера необходим аргумент `-t`.

При запуске контейнера `'создания задач'` обязателен аргумент ``-i`` для возможности вводить значения в терминал.

Можно запускать любое кол-во контейнеров `'симуляции дронов'`.

Для удобной работы рекомендуется запускать каждый контейнер в разных окнах консолей.

## Создание задачи
Задачи создаются на перемещение из одной точки в другую. Всего в нынешней конфигурации среды для дронов-курьеров существует 5 точек `(a, b, c, d, e)`. Они представлены в виде ненаправленого графа.  
[PDF файл с визуализацией графа (graphVisualisation)](graphVisualisation.pdf)

После запуска контейнера `'создания задач'` вы получите следующий вывод:  
```console
root@als-E202SA:/home/als/deliSoft# docker run -it addtask



To add task type: addtask %taskId% %departure% %destination%
For example:
------
2a23fac d b
------
To see not taken tasks, type: tasks



->: 
```
Теперь есть возможность вводить две команды:  


```->: addtask taskidnoone a b``` - Создаёт задачу на перемещение дрона из точки `a` в точку `b`.  
```taskidnoone``` - id задачи. Должен быть уникален.    
```a``` - точка начала полёта  
```b``` - точка завершения полёта


```->: tasks``` - Выводит список всех непринятых задач

Примеры работы
```console 
->: addtask firstTask b e
->: addtask secondTask e c
->: addtask thirdTask a b
->: addtask simpleTaskName e b
->: addtask crazyTask d e
->: tasks
  - id: firstTask,  from: b,  to:e
  - id: secondTask,  from: e,  to:c
  - id: thirdTask,  from: a,  to:b
  - id: simpleTaskName,  from: e,  to:b
->:
```
Разбор:  
Было создано 5 задач. Задачи `firstTask, secondTask, thirdTask, simpleTaskName` не были приняты дронами, а задача `crazyTask` на момент ввода команды `task` была принята одним из дронов в работу.