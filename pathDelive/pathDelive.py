# По всем вопросам обращаться:
# telegram: @vvozian
# vk.com/vozianv

# Демо версия. Код не претендует на production

import socket
import json
import io

read_file = open('paths.json', 'r').read().replace('\n', ' ')
paths = json.loads(read_file)

read_file = open('graph.json', 'r').read().replace('\n', ' ')
graph = json.loads(read_file)

vertices = list(paths)


sor = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP) # UDP

# Enable port reusage so we will be able to run multiple clients and servers on single (host, port). 
# Do not use socket.SO_REUSEADDR except you using linux(kernel<3.9): goto https://stackoverflow.com/questions/14388706/how-do-so-reuseaddr-and-so-reuseport-differ for more information.
# For linux hosts all sockets that want to share the same address and port combination must belong to processes that share the same effective user ID!
# So, on linux(kernel>=3.9) you have to run multiple servers and clients under one user to share the same (host, port).
# Thanks to @stevenreddie
sor.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Enable broadcasting mode
sor.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

sor.bind(("", 37020))
while True:
    # Thanks @seym45 for a fix
    data, addr = sor.recvfrom(4096)
    data = data.decode("utf-8")
    try:
        data = json.loads(str(data))
        if "datatype" in data:
            if data["datatype"] == "getPath":
                if "from" in data and "to" in data:
                    if data["from"] in vertices and data["to"] in vertices and data["from"] != data["to"]:
                        path = paths[data["from"]][data["to"]]
                        ret = {}
                        ret["datatype"] = "pathReturn"
                        ret["path"] = path
                        ret["dists"] = []
                        for i in range(len(path)-1):
                            ret["dists"].append(graph[path[i]]["dist"][path[i+1]])
                        sor.sendto((json.dumps(ret)).encode('utf-8'), (addr[0], 37020))
                        print(f"Path was sended to drone {addr[0]}")
                    else:    
                        ret = {}
                        ret["datatype"] = "error"
                        ret["data"] = 1  # timestamp or task error
                        sor.sendto((json.dumps(ret)).encode('utf-8'), (addr[0], 37020))
                else:    
                    ret = {}
                    ret["datatype"] = "error"
                    ret["data"] = 1  # timestamp or task error
                    sor.sendto((json.dumps(ret)).encode('utf-8'), (addr[0], 37020))
        else:    
            ret = {}
            ret["datatype"] = "error"
            ret["data"] = 1  # timestamp or task error
            sor.sendto((json.dumps(ret)).encode('utf-8'), (addr[0], 37020))
    except:
        ret = {}
        ret["datatype"] = "error"
        ret["data"] = 1  # timestamp or task error
        sor.sendto((json.dumps(ret)).encode('utf-8'), (addr[0], 37020))
