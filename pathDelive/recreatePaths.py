import json
vertices = {'a': '0', 'b': '1', 'c': '2', 'd': '3', 'e': '4'}

read_file = open('paths.json', 'r').read().replace('\n', ' ')
read_file = str(json.loads(read_file))
for i in list(vertices):
    print(f"{vertices[i]} -> {i}")
    read_file = read_file.replace(vertices[i], i)
        
print(read_file)